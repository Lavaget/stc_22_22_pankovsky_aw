package stream;

import java.io.IOException;
import java.rmi.UnexpectedException;

public class Main {
    public static void main(String[] args) {
        ProductRepository productRepository = new ProductsRepositoryFileBasedImpl("C:\\Users\\Андрей Паньковский\\Desktop\\stc_22_22_pankovsky_AW\\AW\\src\\stream\\input.txt");
        try {
            System.out.println(productRepository.findById(2));
            System.out.println("============");
            System.out.println(productRepository.findById(6));
            System.out.println("============");
            System.out.println(productRepository.findById(7));
            System.out.println("============");
            System.out.println(productRepository.findAllByTitleLike("сол"));
            System.out.println("============");
            System.out.println(productRepository.findAllByTitleLike("оло"));
        } catch (IOException ex) {
            System.out.println("Error!");
        }

    }
}
