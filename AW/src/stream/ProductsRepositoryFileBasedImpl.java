package stream;

import java.io.*;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductRepository {

    private final String file; //

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double cost = Double.valueOf(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, name, cost, count);
    };
    private static final Function<Product, String> productToStringMapper = product -> {
        return product.id().toString() + "|" + product.name() + "|" + product.cost() + "|" + product.count();
    };

    public ProductsRepositoryFileBasedImpl(String file) {
        this.file = file;
    }

    @Override
    public Product findById(Integer id) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            return reader.lines().map(stringToProductMapper).filter(it -> it.id().equals(id)).findFirst().get();
        } catch (IOException ex) {
            throw new IOException();
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.name().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException ex) {
            throw new IOException();
        }
    }

    @Override
    public void update(Product product) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file));
             FileWriter writer = new FileWriter(file, false)) {
            //1.достали
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            Product oldProduct = productList.stream().filter(it -> it.id().equals(product.id())).findFirst().get();
            //2.обновили
            Product newProduct = new Product(oldProduct.id(), product.name(), product.cost(), product.count());
            //3.положили
            List<Product> products = productList.stream().map(it -> {
                if (it.id() == newProduct.id()) {
                    return newProduct;
                }
                return it;
            }).toList();
            saveAll(products);
        } catch (UnsuccessfulWorkWithFileException ex) {
            throw new RuntimeException();
        }
    }

    public void save(Product product) throws IOException, UnsuccessfulWorkWithFileException {
        try (FileWriter fileWriter = new FileWriter(file, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = productToStringMapper.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    public void saveAll(List<Product> products) throws IOException, UnsuccessfulWorkWithFileException {
        try (FileWriter fileWriter = new FileWriter(file, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringsProduct = new StringBuilder();
            for (Product product : products) {
                stringsProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringsProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

}




